package com.example.mvp.weatherforecast

import com.example.mvp.weatherforecast.utils.AppUtils
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class WeatherForecastUnitTest {
    @Test
    fun checkDateConverter() {
        val timpStamp : Long = 1652176800
        assertEquals("10/05/2022 17:00", AppUtils.convertDate(timpStamp))
    }

    @Test
    fun checkNumberFormatDown() {
        val num : Double = 1000.123
        assertEquals(1000.12, AppUtils.formatTwoDecimal(num), 0.0)
    }

    @Test
    fun checkNumberFormatUp() {
        val num : Double = 1000.125
        assertEquals(1000.13, AppUtils.formatTwoDecimal(num), 0.0)
    }
}