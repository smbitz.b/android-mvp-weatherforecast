package com.example.mvp.weatherforecast

import android.app.Application
import com.example.mvp.weatherforecast.data.source.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(){
            androidContext(this@WeatherApplication)
            modules(mainModule)
        }
    }
}