package com.example.mvp.weatherforecast.ui.listbinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mvp.weatherforecast.data.model.City
import com.example.mvp.weatherforecast.data.model.WeatherChild
import com.example.mvp.weatherforecast.ui.main.MainAdapter
import com.example.mvp.weatherforecast.ui.result.ResultWeatherForecastAdapter

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<City>?) {
    items?.let {
        (listView.adapter as MainAdapter).submitList(items)
    }
}

@BindingAdapter("app:itemsWeather")
fun setItemsWeather(listView: RecyclerView, items: List<WeatherChild>?) {
    items?.let {
        (listView.adapter as ResultWeatherForecastAdapter).submitList(items)
    }
}