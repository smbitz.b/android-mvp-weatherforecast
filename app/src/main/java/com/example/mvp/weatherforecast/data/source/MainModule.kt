package com.example.mvp.weatherforecast.data.source

import com.example.mvp.weatherforecast.viewmodel.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single<AppDataRepository> { AppDataRepositoryImpl() }

    viewModel { MainViewModel(get(), androidContext()) }
    viewModel { ResultViewModel(get(), androidContext()) }
    viewModel { ResultWeatherCurrentViewModel() }
    viewModel { ResultWeatherForecastViewModel() }
    viewModel { SharedViewModel() }
}