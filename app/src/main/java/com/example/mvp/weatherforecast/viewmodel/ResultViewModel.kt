package com.example.mvp.weatherforecast.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvp.weatherforecast.data.model.Weather
import com.example.mvp.weatherforecast.data.source.AppDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ResultViewModel(private val repo : AppDataRepository, context: Context) : ViewModel() {

    var weather = MutableLiveData<Weather>()
    var isLoading = MutableLiveData<Boolean>()
    var isTempCelsius = MutableLiveData<Boolean>().apply { postValue(true) }

    fun getWeatherData(lat: Float, lon: Float) {
        viewModelScope.launch(Dispatchers.IO) {
            weather.postValue(repo.getCityWeatherData(lat, lon))
        }
    }

    fun onTempTypeChange(isChecked: Boolean) {
        isTempCelsius.value = isChecked
    }

}