package com.example.mvp.weatherforecast.data.api

import com.example.mvp.weatherforecast.common.Constants
import com.example.mvp.weatherforecast.data.model.Weather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    @GET("onecall")
    fun oneCall(
        @Query("lat") lat: Float,
        @Query("lon") lon: Float,
        @Query("exclude") exclude: String = Constants.OPEN_WEATHER_EXCLUDE,
        @Query("appid") appId: String = Constants.OPEN_WEATHER_API_KEY,
        @Query("units") metric: String = Constants.OPEN_WEATHER_DEFAULT_UNIT,
    ): Call<Weather>

}