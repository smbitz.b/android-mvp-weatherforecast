package com.example.mvp.weatherforecast.data.model

data class Weather (
    val current: WeatherChild,
    val hourly: List<WeatherChild>
)

data class WeatherChild (
    val dt: Long,
    var temp: Double,
    var tempDefault: Double,
    val humidity: Int,
    var dtLabel: String
)