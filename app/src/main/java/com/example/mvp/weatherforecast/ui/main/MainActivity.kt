package com.example.mvp.weatherforecast.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mvp.weatherforecast.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}