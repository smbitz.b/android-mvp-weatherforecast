package com.example.mvp.weatherforecast.viewmodel

import android.content.Context
import android.text.Editable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvp.weatherforecast.data.model.City
import com.example.mvp.weatherforecast.data.source.AppDataRepository

class MainViewModel(private val repo : AppDataRepository, context: Context) : ViewModel() {
    var citiesDefault = repo.getCities(context)
    var cities = MutableLiveData<List<City>>().apply { postValue(citiesDefault) }

    fun searchCity(text :Editable){
        cities.value = citiesDefault.filter { it.name.lowercase().contains(text.toString().lowercase()) }
    }
}