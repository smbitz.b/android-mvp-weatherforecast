package com.example.mvp.weatherforecast.data.source

import android.content.Context
import com.example.mvp.weatherforecast.data.model.City
import com.example.mvp.weatherforecast.data.model.Weather

interface AppDataRepository {
    fun getCities(context: Context): List<City>
    fun getCityWeatherData(lat: Float, lon: Float): Weather?
}