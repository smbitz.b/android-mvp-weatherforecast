package com.example.mvp.weatherforecast.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class City(
    val id: Int,
    val name: String,
    val coord: Coord
) : Parcelable

@Parcelize
data class Coord(
    val lat: Float,
    val lon: Float
) : Parcelable
