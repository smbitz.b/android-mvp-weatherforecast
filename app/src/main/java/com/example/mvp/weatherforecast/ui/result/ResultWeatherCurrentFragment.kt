package com.example.mvp.weatherforecast.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.mvp.weatherforecast.databinding.ResultWeatherCurrentFragmentBinding
import com.example.mvp.weatherforecast.viewmodel.ResultWeatherCurrentViewModel
import com.example.mvp.weatherforecast.viewmodel.SharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResultWeatherCurrentFragment : Fragment() {

    companion object {
        fun newInstance() = ResultWeatherCurrentFragment()
    }

    private val viewModel: ResultWeatherCurrentViewModel by viewModel()
    private val sharedViewModel: SharedViewModel by activityViewModels()

    private lateinit var binding: ResultWeatherCurrentFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ResultWeatherCurrentFragmentBinding.inflate(inflater, container, false)
        binding.apply {
            this.viewmodel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
    }

    private fun setupObserver() {
        binding.lifecycleOwner = this.viewLifecycleOwner
        sharedViewModel.weather.observe(viewLifecycleOwner) {
            viewModel.cityName.value = sharedViewModel.cityName.value
            viewModel.cityTemp.value = it.current.temp
            viewModel.cityTempDefault = it.current.temp
            viewModel.cityhumidity.value = it.current.humidity.toString()
        }
        sharedViewModel.isTempCelsius.observe(viewLifecycleOwner) {
            viewModel.calTempLabel(it)
        }
    }

}