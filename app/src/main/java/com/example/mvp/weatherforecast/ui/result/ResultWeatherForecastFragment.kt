package com.example.mvp.weatherforecast.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.mvp.weatherforecast.databinding.ResultWeatherForecastFragmentBinding
import com.example.mvp.weatherforecast.viewmodel.ResultWeatherForecastViewModel
import com.example.mvp.weatherforecast.viewmodel.SharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResultWeatherForecastFragment : Fragment() {

    companion object {
        fun newInstance() = ResultWeatherForecastFragment()
    }

    private val viewModel: ResultWeatherForecastViewModel by viewModel()
    private val sharedViewModel: SharedViewModel by activityViewModels()

    private lateinit var binding: ResultWeatherForecastFragmentBinding
    private lateinit var listAdapter: ResultWeatherForecastAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ResultWeatherForecastFragmentBinding.inflate(inflater, container, false)
        binding.apply {
            this.viewmodel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListAdapter()
        setupObserver()
    }

    private fun setupListAdapter() {
        listAdapter = ResultWeatherForecastAdapter(){}
        binding.rvWeatherList.adapter = listAdapter
    }

    private fun setupObserver() {
        binding.lifecycleOwner = this.viewLifecycleOwner
        sharedViewModel.weather.observe(viewLifecycleOwner, Observer {
            viewModel.weatherHourly.value = it.hourly
            listAdapter.submitList(viewModel.weatherHourly.value)
        })
        sharedViewModel.isTempCelsius.observe(viewLifecycleOwner) {
            listAdapter.setIsTempCelsius(it)
        }
    }

}