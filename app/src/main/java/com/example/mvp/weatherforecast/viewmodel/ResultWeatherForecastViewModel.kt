package com.example.mvp.weatherforecast.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvp.weatherforecast.data.model.WeatherChild

class ResultWeatherForecastViewModel : ViewModel() {

    var weatherHourly = MutableLiveData<List<WeatherChild>>()

}