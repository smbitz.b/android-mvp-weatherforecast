package com.example.mvp.weatherforecast.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvp.weatherforecast.data.model.Weather

class SharedViewModel : ViewModel() {

    var cityName = MutableLiveData<String>()
    var weather = MutableLiveData<Weather>()
    var isTempCelsius = MutableLiveData<Boolean>().apply { postValue(true) }

}