package com.example.mvp.weatherforecast.data.source

import android.content.Context
import com.example.mvp.weatherforecast.common.Constants
import com.example.mvp.weatherforecast.data.api.WeatherAPI
import com.example.mvp.weatherforecast.data.model.City
import com.example.mvp.weatherforecast.data.model.Weather
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class AppDataRepositoryImpl() : AppDataRepository {
    override fun getCities(context: Context): List<City> {
        lateinit var jsonString: String
        try {
            jsonString = context.assets.open(Constants.ASSET_CITY)
                .bufferedReader()
                .use { it.readText() }
        } catch (ioException: IOException) {
            // AppLogger.d(ioException)
        }

        val listCity = object : TypeToken<List<City>>() {}.type
        return Gson().fromJson(jsonString, listCity)
    }

    override fun getCityWeatherData(lat: Float, lon: Float): Weather? {
        val service: WeatherAPI = getNetworkClient().create(WeatherAPI::class.java)

        return service
            .oneCall(lat = lat, lon = lon)
            .execute().body()
    }

    private fun getNetworkClient(): Retrofit{
        return Retrofit.Builder()
            .baseUrl(Constants.OPEN_WEATHER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}