package com.example.mvp.weatherforecast.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvp.weatherforecast.utils.AppUtils

class ResultWeatherCurrentViewModel : ViewModel() {

    var cityName = MutableLiveData<String>().apply { postValue("-") }
    var cityTemp = MutableLiveData<Double>().apply { postValue(0.0) }
    var cityhumidity = MutableLiveData<String>().apply { postValue("0") }

    var cityTempDefault : Double = 0.0

    fun calTempLabel(isCheck: Boolean){
        if(cityTemp.value!! > 0) {
            val tempLabel = if (isCheck) {
                cityTempDefault
            }else {
                AppUtils.convertCelsiusToFahrenheit(cityTemp.value!!)
            }
            cityTemp.value = tempLabel
        }
    }

}