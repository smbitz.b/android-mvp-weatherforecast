package com.example.mvp.weatherforecast.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.mvp.weatherforecast.R
import com.example.mvp.weatherforecast.common.Constants
import com.example.mvp.weatherforecast.databinding.MainFragmentBinding
import com.example.mvp.weatherforecast.ui.result.ResultActivity
import com.example.mvp.weatherforecast.utils.AppUtils
import com.example.mvp.weatherforecast.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()

    private lateinit var binding: MainFragmentBinding
    private lateinit var listAdapter: MainAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.apply {
            this.viewmodel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListAdapter()
        setupObserver()
    }

    private fun setupListAdapter() {
        listAdapter = MainAdapter() { item ->
            this.context?.let {
                if (AppUtils.isOnline(it)){
                    val intent = Intent(it, ResultActivity::class.java)
                    intent.putExtra(Constants.INTENT_DATA_CITY, item)
                    startActivity(intent)
                }else{
                    Toast.makeText(it, R.string.label_no_internet, Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.rvCityList.adapter = listAdapter
    }

    private fun setupObserver() {
        binding.lifecycleOwner = this.viewLifecycleOwner
        viewModel.cities.observe(viewLifecycleOwner, Observer {
            listAdapter.submitList(it)
        })
    }

}