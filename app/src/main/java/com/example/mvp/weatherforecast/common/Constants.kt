package com.example.mvp.weatherforecast.common

object Constants {
    const val ASSET_CITY = "city.list.min.json"

    const val INTENT_DATA_CITY = "INTENT_DATA_CITY"

    const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
    const val OPEN_WEATHER_API_KEY = "c9e553217a2f6102daf99374a9f2cbf4"
    const val OPEN_WEATHER_EXCLUDE = "minutely,daily,alerts"
    const val OPEN_WEATHER_DEFAULT_UNIT = "metric"
}