package com.example.mvp.weatherforecast.ui.result

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mvp.weatherforecast.data.model.WeatherChild
import com.example.mvp.weatherforecast.databinding.ResultWeatherForecastItemBinding
import com.example.mvp.weatherforecast.utils.AppUtils

class ResultWeatherForecastAdapter(private val listener: (WeatherChild) -> Unit) :
    ListAdapter<WeatherChild, ResultWeatherForecastAdapter.ViewHolder>(WeatherChildDiffCallback()) {

    private var isTempCelsius = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, isTempCelsius)
        holder.itemView.setOnClickListener{ listener(item) }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setIsTempCelsius(isTempCelsius : Boolean){
        this.isTempCelsius = isTempCelsius
        this.notifyDataSetChanged()
    }

    class ViewHolder private constructor(private val binding: ResultWeatherForecastItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: WeatherChild, isTempCelsius: Boolean) {
            val tempLabel = if (isTempCelsius) {
                if (item.tempDefault == 0.0) item.tempDefault = item.temp
                item.tempDefault
            }else{
                AppUtils.convertCelsiusToFahrenheit(item.tempDefault)
            }

            item.dtLabel = AppUtils.convertDate(item.dt)
            item.temp = tempLabel

            binding.weatherChild = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ResultWeatherForecastItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class WeatherChildDiffCallback : DiffUtil.ItemCallback<WeatherChild>() {
    override fun areItemsTheSame(oldItem: WeatherChild, newItem: WeatherChild): Boolean {
        return oldItem.dt == newItem.dt
    }

    override fun areContentsTheSame(oldItem: WeatherChild, newItem: WeatherChild): Boolean {
        return oldItem == newItem
    }
}