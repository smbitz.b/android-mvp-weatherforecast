package com.example.mvp.weatherforecast.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class AppUtils {

    companion object {
        fun convertDate(time: Long) : String {
            val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale("th"))
            val date = Date(time * 1000)
            return sdf.format(date)
        }

        fun convertCelsiusToFahrenheit (num: Double) : Double{
            return formatTwoDecimal(num.times(1.8).plus(32))
        }

        fun formatTwoDecimal(num: Double) : Double{
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.HALF_UP
            return df.format(num).toDouble()
        }

        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    return true
                }
            }
            return false
        }
    }

}