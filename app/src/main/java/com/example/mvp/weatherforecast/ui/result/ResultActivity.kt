package com.example.mvp.weatherforecast.ui.result

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.mvp.weatherforecast.R
import com.example.mvp.weatherforecast.common.Constants
import com.example.mvp.weatherforecast.data.model.City
import com.example.mvp.weatherforecast.databinding.ResultActivityBinding
import com.example.mvp.weatherforecast.viewmodel.ResultViewModel
import com.example.mvp.weatherforecast.viewmodel.SharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResultActivity : AppCompatActivity() {

    private val viewModel: ResultViewModel by viewModel()
    private val sharedViewModel: SharedViewModel by viewModel()

    private lateinit var binding: ResultActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ResultActivityBinding.inflate(layoutInflater)
        binding.apply {
            this.viewmodel = viewModel
        }
        setContentView(binding.root)

        binding.bottomNv.setupWithNavController(findNavController(R.id.nav_host_fragment_result))

        setupObserver()
        loadCityWeatherData()
    }

    private fun setupObserver() {
        binding.lifecycleOwner = this
        viewModel.weather.observe(this) {
            sharedViewModel.weather.value = it
            viewModel.isLoading.value = false
        }
        viewModel.isTempCelsius.observe(this) {
            sharedViewModel.isTempCelsius.value = it
        }
    }

    private fun loadCityWeatherData() {
        viewModel.isLoading.value = true

        val city = intent.getParcelableExtra<City>(Constants.INTENT_DATA_CITY)!!
        city.coord.let { viewModel.getWeatherData(it.lat, it.lon) }
        sharedViewModel.cityName.value = city.name
    }
}