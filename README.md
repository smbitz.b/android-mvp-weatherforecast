# Android-MVP-WeatherForecast

The MVP (Minimum Viable Product) of Weather forecast that use library below.

1. Kotlin language
2. Design Pattern with MVVM
3. Use Dependency Injection (Koin)
4. Use Coroutine
5. Constraint layout
6. Navigation component
7. View binding, Data binding
8. Unit test